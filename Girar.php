<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Gira</title>
  </head>
  <body>
  <?php
     if(!$_POST['girar']){
      $i = 0;
      $lineal = [];
      $n = 1;
      do {
       
        $lineal[] = $n;
        $i++;
        $n++;
        
      } while ($i < 144);
      $minimo = 999;
      $i = 0;
      for ($x = 0; $x < 12; $x++) {
        for ($y = 0; $y < 12; $y++) {
          $numero[$x][$y] = $lineal[$i];
          $i++;
        }
      }

      echo "<table>";
      for ($x = 0; $x < 12; $x++) {
        echo "<tr>";
        for ($y = 0; $y < 12; $y++) {
          
            echo '<td>'.$numero[$x][$y].'</td>';
          
        }
        echo "</tr>";
      }
      echo "</table>";
    ?>
    
    
    <form action="Girar.php" method="POST">
        <input type="hidden" name="girar" value="<?php echo true; ?>">
        <?php
            echo '<input type="hidden" name="itemsArr" value="'.json_encode($numero). '">';
        ?>
        <input type="submit" value="Girar">
    </form>
    <?php
    }
    else {
    ?>
    <?php
    $numero = json_decode($_POST['itemsArr'],true);
    
    $principio = 0;
    $final = 12;
    for($anillo = 0; $anillo < 6; $anillo++){
        for($j = $principio, $y = ($final - 1); $j< ($final - 1);$j++, $y--){
            if($j == $principio){
                $n2 = $numero[$principio][$j];
                $n1 = $numero[($final - 1)][$y];
            }
            $numero[$principio][$j] = $numero[$principio][($j + 1)];
            $numero[($final - 1)][$y] = $numero[($final -1)][($y-1)];
        }
        for($z = ($final - 1), $n = $principio; $n < ($final - 1); $z--, $n++){
            if($z == ($principio + 1)){
                $numero[$z][$principio] = $n2;
                $numero[$n][($final - 1)] = $n1;
            }
            else{
                $numero[$z][$principio] = $numero[($z - 1)][$principio];
                $numero[$n][($final - 1)] = $numero[($n + 1)][($final -1)];
            }
        } 
        $principio++;
        $final--;
    }

    echo "<table>";
    for ($x = 0; $x < 12; $x++) {
      echo "<tr>";
      for ($y = 0; $y < 12; $y++) {
        
          echo '<td>'.$numero[$x][$y].'</td>';
        
      }
      echo "</tr>";
    }
    echo "</table>";
    ?>
    <form action="Girar.php" method="POST">
        <input type="hidden" name="girar" value="<?php echo true; ?>">
        <?php
            echo '<input type="hidden" name="itemsArr" value="'.json_encode($numero). '">';
        ?>
        <input type="submit" value="Girar">
    </form>
    <?php
    }
    ?>
  </body>
</html>