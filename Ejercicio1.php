<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DisFruta</title>
</head>
<body>
    <?php

        function numeroFrutas($fruta, $array){
            $contador = 0;
            for($i=0; $i<count($array);$i++){
                if($fruta == $array[$i]){
                    $contador++;
                }
            }
            
            return $contador;
        }
    ?>
    <h1>DisFruta con PHP</h1>

    <h2><?php $numero = rand(7, 20); echo $numero; ?> frutas</h2>

    <p style="font-size: 7rem">
        <?php
            
            for($i = 0; $i < $numero; $i++){
                $fruta = rand(127815, 127827);
                echo '&#'.$fruta;
                $arrayFrutas[$i] = $fruta;
            }
        ?>
    
    </p>

    <h2>Resultados</h2>
    <?php
        $arraySinRepetir = [];
        $contador = 0;
        for($i = 0; $i < count($arrayFrutas); $i++){
            if(!in_array($arrayFrutas[$i], $arraySinRepetir)){
                array_push($arraySinRepetir, $arrayFrutas[$i]);
                echo "<p>La fruta <span style='font-size: 2rem'>&#".$arraySinRepetir[$contador];
                $contador++;
                
                    if(numeroFrutas($arrayFrutas[$i], $arrayFrutas) > 1){
                        echo "</span> está <strong>".numeroFrutas($arrayFrutas[$i], $arrayFrutas)."</strong> veces en la lista.</p>";
                    }else{
                        echo "</span> está <strong>1</strong> vez en la lista.</p>";
                    }  
            }
        }
    ?>
    <br>
    <button onclick="location.reload()">¡¡¡ DisFruta otra vez !!!</button>
    <br>

</body>
</html>